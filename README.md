# PadNotify.py

## Overview
**PadNotify** is a small Python script that notifies your Etherpad-lite users about changes in their pads using Prowl.


## Requirements
 - Python 2.7
 - Bottle.py
 - Requests
 - MySQLdb
 
Due to the very limited API of Etherpad-lite, PyNotify needs to have direct access to the MySQL database used by Etherpad.


## Bugs
Code and web interface are currently as ugly as hell. Feel free to beautify and create a pull request or a fork.


## License
Do what ever you want, I don't care. If you'd like to use the - maybe modified - script, I'd love you to donate some money for charity. Or just get me a beer.
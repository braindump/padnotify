#!/usr/bin/env python2.7
#
# Author: Lars Herbach, lars@freistil.cz, http://freistil.cz


import MySQLdb
import time
import requests
from bottle import route, run, request
from thread import start_new_thread

########################################
#
# Config
#
########################################

# URL of the pad
pad_url                   = ""
# Verbose Name of pad
pad_name                  = ""
# API key, found in APIKEY.txt
pad_api_key               = ""
# Currently just 1
pad_api_version           = 1
# Database, the pad uses
pad_db_user               = ""
pad_db_passwd             = ""
pad_db_database           = ""
pad_db_host               = ""
# Database, the notification system should use
notifications_db_user     = ""
notifications_db_passwd   = ""
notifications_db_database = ""
notifications_db_host     = ""
# Port of notification web interface
notifications_port        = "9038"
# Polling frequency in secons
check_interval            = 180


########################################
#
# Web interface
#
########################################

@route("/")
def web_index_form():
    retVal  = "<html>"
    retVal += "  <head>"
    retVal += "    <title>"
    retVal += pad_name + " Notification Center"
    retVal += "    </title>"
    retVal += "  </head>"
    retVal += "  <body>"
    retVal += "    <form method='POST'>"
    retVal += "      Pad ID:"
    retVal += "      <input name='padID' type='text' /><br />"
    retVal += "      Prowl API Key:"
    retVal += "      <input name='prowlkey' type='text' /><br />"
    retVal += "      Pad Alias for Notification:"
    retVal += "      <input name='padalias' type='text' /><br />"
    retVal += "      Add"
    retVal += "      <input type='radio' name='whattodo' value='addNot' />"
    retVal += "      Remove"
    retVal += "      <input type='radio' name='whattodo' value='delNot' /><br/>"
    retVal += "      <input type='submit' value=' Submit ' />"
    retVal += "    </form>"
    retVal += "  </body>"
    retVal += "</html>"
    return retVal

@route("/", method="POST")
def web_index_submit():
    if request.forms.get("whattodo") == "addNot" \
       and (request.forms.get("padID") \
       and request.forms.get("prowlkey") \
       and request.forms.get("padalias")):
        status = addNotification(pad_id     = request.forms.get("padID"),
                                 prowl_key  = request.forms.get("prowlkey"),
                                 alias      = request.forms.get("padalias"))
    elif request.forms.get("whattodo") == "delNot" \
       and (request.forms.get("padID") \
       and request.forms.get("prowlkey")):
        status = delNotification(pad_id     = request.forms.get("padID"),
                                 prowl_key  = request.forms.get("prowlkey"))
    retVal  = "<html>"
    retVal += "  <head>"
    retVal += "    <title>"
    retVal += pad_name + " Notification Center"
    retVal += "    </title>"
    retVal += "  </head>"
    retVal += "  <body>"
    retVal += str(status)
    retVal += "    <br /><br /><br />"
    retVal += "    <form method='POST'>"
    retVal += "      Pad ID:"
    retVal += "      <input name='padID' type='text' /><br />"
    retVal += "      Prowl API Key:"
    retVal += "      <input name='prowlkey' type='text' /><br />"
    retVal += "      Pad Alias for Notification:"
    retVal += "      <input name='padalias' type='text' /><br />"
    retVal += "      Add"
    retVal += "      <input type='radio' name='whattodo' value='addNot' />"
    retVal += "      Remove"
    retVal += "      <input type='radio' name='whattodo' value='delNot' /><br/>"
    retVal += "      <input type='submit' value=' Submit ' />"
    retVal += "    </form>"
    retVal += "  </body>"
    retVal += "</html>"
    return retVal


########################################
#
# Functions
#
########################################

def addNotification(pad_id, prowl_key, alias):
    conn = MySQLdb.connect(host    = notifications_db_host,
                           user    = notifications_db_user,
                           passwd  = notifications_db_passwd,
                           db      = notifications_db_database)
    cursor = conn.cursor()
    row = cursor.execute("SELECT id FROM epn_notifications WHERE pad=%s AND prowl=%s;",
                        (pad_id, prowl_key))
    if row > 0:
        retVal = "Sorry, there is already a notification for this Pad ID and Prowl API Key."
    else:
        cursor.execute("INSERT INTO epn_notifications (pad, prowl, alias) VALUES (%s, %s, %s)",
                      (pad_id, prowl_key, alias))
        row = cursor.execute("SELECT id FROM epn_pads WHERE pad=%s;", (pad_id,))
        if row == 0:
            cursor.execute("INSERT INTO epn_pads (pad, rev) VALUES (%s, 0);", (pad_id,))
        retVal = "Notification set."
    cursor.close()
    conn.close()
    return retVal

def delNotification(pad_id, prowl_key):
    conn = MySQLdb.connect(host    = notifications_db_host,
                           user    = notifications_db_user,
                           passwd  = notifications_db_passwd,
                           db      = notifications_db_database)
    cursor = conn.cursor()
    cursor.execute("DELETE FROM epn_notifications WHERE pad=%s AND prowl=%s",
                  (pad_id, prowl_key))
    retVal = "Notification removed."
    cursor.close()
    conn.close()
    return retVal

def createNotifications(oldrow, authors):
    padKey = "pad:" + str(oldrow[1]) + ":revs:" + str(oldrow[2])
    conn = MySQLdb.connect(host    = notifications_db_host,
                           user    = notifications_db_user,
                           passwd  = notifications_db_passwd,
                           db      = notifications_db_database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM store")
    rows = cursor.fetchall()
    for row in rows:
        if padKey in row[0]:
            author = authors[row[1].split(",")[1].split(":")[2][1:-1]]
            if not author:
                author = "Unknown"
            break
    cursor.execute("SELECT prowl, alias FROM epn_notifications WHERE pad=%s", (oldrow[1],))
    rows = cursor.fetchall()
    for row in rows:
        start_new_thread(sendNotification, (oldrow[1], str(row[0]), str(row[1]), author))
    cursor.close()
    conn.close()

def sendNotification(pad, prowl, alias, author):
    prowlRequest      = "https://api.prowlapp.com/publicapi/add?"
    prowlRequest     += "apikey="
    prowlRequest     += prowl
    prowlRequest     += "&url="
    prowlRequest     += pad_url + "/p/" + pad
    prowlRequest     += "&application="
    prowlRequest     += alias.replace(" ", "%20")
    prowlRequest     += "&description=New%20Entry%20by%20"
    prowlRequest     += author
    requests.post(prowlRequest)

def notificationChecker():
    revisionsRequest  = str(pad_url)
    revisionsRequest += "/api/"
    revisionsRequest += str(pad_api_version)
    revisionsRequest += "/getRevisionsCount?apikey="
    revisionsRequest += str(pad_api_key)
    revisionsRequest += "&padID="
    authorKey         = "globalAuthor:"
    authors = {}
    while (True):
        conn = MySQLdb.connect(host    = notifications_db_host,
                               user    = notifications_db_user,
                               passwd  = notifications_db_passwd,
                               db      = notifications_db_database)
        cursor = conn.cursor()
        cursor.execute("SELECT * from store")
        rows = cursor.fetchall()
        for row in rows:
            if authorKey in row[0]:
                if not row[0].split(":")[1] in authors:
                    authorHash = row[0].split(":")[1]
                    authorName = row[1].split(",")[1].split(":")[1][1:-1]
                    if not authorName == "ul":
                        authors[authorHash] = authorName
        cursor.execute("SELECT * FROM epn_pads")
        rows = cursor.fetchall()
        for row in rows:
            req = requests.get(revisionsRequest + str(row[1]))
            currentRevision = int(req.text.split(",")[2].split(":")[2][:-2])
            if currentRevision > int(row[2]):
                cursor.execute("UPDATE epn_pads SET rev=%s WHERE pad=%s",
                              (currentRevision, row[1]))
                start_new_thread(createNotifications, (row, authors))       
        cursor.close()
        conn.close()
        time.sleep(check_interval)

if __name__ == "__main__":
    start_new_thread(run, (None, "wsgiref", "localhost", int(notifications_port),))
    start_new_thread(notificationChecker, ())


    while (True):
        try:
            time.sleep(30)
        except (KeyboardInterrupt, SystemExit):
            print "Quitting..."
            raise